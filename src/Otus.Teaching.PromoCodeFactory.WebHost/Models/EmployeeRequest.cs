﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        [Required(ErrorMessage = "Поле FirstName обязательно к заполнению")]
        public string FirstName { get; set; }
        
        [Required(ErrorMessage = "Поле LastName обязательно к заполнению")]
        public string LastName { get; set; }
        
        [Required(ErrorMessage = "Поле Email обязательно к заполнению")]
        [EmailAddress(ErrorMessage = "Email введен неверно.")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "Поле Roles обязательно к заполнению")]
        public List<Role> Roles { get; set; }
        
        [Required(ErrorMessage = "Поле AppliedPromocodesCount обязательно к заполнению")]
        public int AppliedPromocodesCount { get; set; }
    }
}