﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создание сотрудника
        /// </summary>
        /// <param name="employeeRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Employee>> CreateEmployeeAsync(EmployeeRequest employeeRequest)
        {
            var employeeToCreate = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Email = employeeRequest.Email,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
                Roles = employeeRequest.Roles
            };

            var employeeCreated = await _employeeRepository.AddAsync(employeeToCreate);

            return employeeCreated;
        }


        /// <summary>
        /// Изменение данных сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPatch("{id:guid}")]
        public async Task<ActionResult<EmployeeRequest>> UpdateEmployeeAsync(Guid id, EmployeeRequest employeeRequest)
        {
            var employeeToUpdate = await _employeeRepository.GetByIdAsync(id);
            if (employeeToUpdate == null)
            {
                return NotFound();
            }
            
            employeeToUpdate.FirstName = employeeRequest.FirstName;
            employeeToUpdate.LastName = employeeRequest.LastName;
            employeeToUpdate.Email = employeeRequest.Email;
            employeeToUpdate.Roles = employeeRequest.Roles;
            employeeToUpdate.AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount;
            await _employeeRepository.UpdateAsync(employeeToUpdate);

            return employeeRequest;
        }


        /// <summary>
        /// Удаление сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            else
            {
                await _employeeRepository.DeleteAsync(id);
                return Ok();
            }
        }
    }
}